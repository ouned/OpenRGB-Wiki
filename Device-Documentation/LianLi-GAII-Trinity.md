# Lian Li GAII Trinity AIO

Inofficial LED Controller Protocol Specification for Lian Li GAII Trinity AIO  
**Firmware Version:** 0.7 (as shown by LConnect Software)  
**Written by:** Michael Losert <michael@losert.org>  

**USB VID:** 0x0416  
**USB PID:** 0x7373  
  
The device opens the interface at number **0x02** and HID Report ID is **0x01**.

## LED Data Specification

Data Packet (64-bytes, filled with zeros):  

| `00` `01` | `02` `03` `04` `05` `06` `07` `08` `09` | `0A` `0B` `0C` `0D` `0E` `0F` `10` `11` `12` `13` `14` `15` | `16` | `17` `18` |
|-----------|-----------------------------------------|-------------------------------------------------------------|------|-----------|
| `01` `83` | `00` `00` `00` `13` `RR` `MM` `BB` `SS` | `R0` `G0` `B0` `R1` `G1` `B1` `R2` `G2` `B2` `R3` `G3` `B3` | `DD` | `00` `..` |

### RR (ring specifier):

| Value | Description |
|-------|-------------|
| 00    | inner ring  |
| 01    | outer ring  |
| 02    | both rings  |

### MM (mode specifier):

| Value | Mode                  | BB | SS | DD | RGB0 | RGB1 | RGB2 | RGB3 | Limitation                               |
|-------|-----------------------|----|----|----|------|------|------|------|------------------------------------------|
| 01    | Rainbow               | ✓  | ✓  | ✓  |      |      |      |      |                                          |
| 02    | Rainbow Morph         | ✓  | ✓  |    |      |      |      |      |                                          |
| 03    | Static Color          | ✓  |    |    |  ✓   | (✓)  |      |      | RR=02 ? RGB0=inner ring, RGB1=outer ring |
| 04    | Breathing Color       | ✓  | ✓  |    |  ✓   | (✓)  |      |      | RR=02 ? RGB0=inner ring, RGB1=outer ring |
| 05    | Runway                | ✓  | ✓  |    |  ✓   |  ✓   |      |      |                                          |
| 06    | Meteor                | ✓  | ✓  | ✓  |  ✓   |  ✓   |  ✓   |  ✓   |                                          |
| 07    | Vortex                | ✓  | ✓  | ✓  |  ✓   |  ✓   |  ✓   |  ✓   | RR=02                                    |
| 08    | Crossing Over         | ✓  | ✓  | ✓  |  ✓   |  ✓   |  ✓   |  ✓   | RR=02                                    |
| 09    | Tai Chi               | ✓  | ✓  | ✓  |  ✓   |  ✓   |      |      |                                          |
| 0A    | Colorful Starry Night | ✓  | ✓  |    |      |      |      |      |                                          |
| 0B    | Static Starry Night   | ✓  | ✓  |    |  ✓   |      |      |      |                                          |
| 0C    | Voice                 | ✓  | ✓  |    |  ✓   | (✓)  |      |      | RR=02 ? RGB0=inner ring, RGB1=outer ring |
| 0D    | Big Bang              | ✓  | ✓  |    |  ✓   |  ✓   |  ✓   |  ✓   | RR=02                                    |
| 0E    | Pump                  | ✓  | ✓  | ✓  |  ✓   |      |      |      |                                          |
| 0F    | Colors Morph          | ✓  | ✓  | ✓  |      |      |      |      | RR=02                                    |
| 10    | Bounce                | ✓  | ✓  |    |  ✓   |  ✓   |  ✓   |  ✓   | RR=01                                    |

### BB (brightness specifier):

| Value | Description     |
|-------|-----------------|
| 00    | off             |
| 01    | 25% brightness  |
| 02    | 50% brightness  |
| 03    | 75% brightness  |
| 04    | 100% brightness |

### SS (speed specifier):

| Value | Description     |
|-------|-----------------|
| 00    | very slow       |
| 01    | slow            |
| 02    | moderate        |
| 03    | fast            |
| 04    | very fast       |

### RGB (0-3):
4 * 3 bytes of RGB data, which usage is mode-specific

### DD (direction specifier):

| Value | Description     |
|-------|-----------------|
| 00    | right           |
| 01    | left            |

## Firmware Interface Specification

Request Data Packet (64-bytes, filled with zeros):  

| `00` `01` | `02` `03` |
|-----------|-----------|
| `01` `86` | `00` `..` |

Response Data Packet (64-bytes) 1: "M252,01,HS,SQ,CALAHADII_RT,V1.04.02E,0.7"  
Response Data Packet (64-bytes) 2: "Jul 19 2023,20:07:51"  
  
actual response string starts at response[0x06]
