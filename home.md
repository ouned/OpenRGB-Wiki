# ![OpenRGB](uploads/5b7e633ac9f63b00c8a4c72686206c3f/OpenRGB.png)

Welcome to the OpenRGB Wiki.  This wiki is the home of much of the OpenRGB project's documentation.

## Check the README

* Before digging into this wiki, be sure to read through the README in the OpenRGB GitLab project.  It contains information on how to set up OpenRGB on the various supported platforms as well as links to applications and plugins that integrate with OpenRGB.  [OpenRGB README](https://gitlab.com/CalcProgrammer1/OpenRGB/-/blob/master/README.md)

## Supported Devices

* The list of supported devices can be viewed on the OpenRGB website: [Supported Devices](https://openrgb.org/devices.html).

## History of OpenRGB

* OpenRGB is a continuation of OpenAuraSDK, which itself was created out of reverse engineering work done on the Keyboard Visualizer project.  For a complete history of the RGB projects that led to OpenRGB's creation, see the [History page](History-of-OpenRGB.md).

## User Documentation

* [Frequently Asked Questions](User-Documentation/Frequently-Asked-Questions.md)
* [OpenRGB Windows Setup and Usage](User-Documentation/OpenRGB-Windows-Setup-and-Usage.md)
* [Using OpenRGB](User-Documentation/Using-OpenRGB.md)
* Want to configure OpenRGB via the OpenRGB.json settings file?  Check out the [OpenRGB Settings](User-Documentation/OpenRGB-Settings.md) page.
* [Autostart with Flatpak](User-Documentation/Autostart-with-Flatpak.md)

## Developer Documentation

* Want to contribute support for a new device?  Check out the [RGBController API](Developer-Documentation/The-RGBController-API.md) page.
* Want to create a new OpenRGB SDK client implementation?  Check out the [OpenRGB SDK Documentation](Developer-Documentation/OpenRGB-SDK-Documentation.md) page.

## Additional Developer Resources

* [Building OpenRGB on Windows](Developer-Documentation/Building-OpenRGB-on-Windows/Building-OpenRGB-on-Windows.md)
* [Building OpenRGB on WSL](Developer-Documentation/Building-OpenRGB-On-WSL/Building-OpenRGB-on-WSL.md)
* [SMBus Interface Details](Developer-Documentation/SMBus-Interface-Details.md)
* [Reverse Engineering](Developer-Documentation/Reverse-Engineering.md)
* [Motherboard Name From DMI Info](Developer-Documentation/Motherboard-Name-From-DMI-Info.md)
* [USB Captures Using Wireshark](Developer-Documentation/USB-Captures-Using-Wireshark.md)